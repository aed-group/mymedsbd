﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBoxData = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ButtonGetApointments = New System.Windows.Forms.Button()
        Me.ButtonGetPrescriptions = New System.Windows.Forms.Button()
        Me.ButtonGetStock = New System.Windows.Forms.Button()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.ButtonGetInTakes = New System.Windows.Forms.Button()
        Me.ButtonEdit = New System.Windows.Forms.Button()
        Me.ButtonStatistics = New System.Windows.Forms.Button()
        Me.ButtonSupervisees = New System.Windows.Forms.Button()
        Me.ButtonDelUser = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'ListBoxData
        '
        Me.ListBoxData.FormattingEnabled = True
        Me.ListBoxData.Location = New System.Drawing.Point(11, 132)
        Me.ListBoxData.Margin = New System.Windows.Forms.Padding(2)
        Me.ListBoxData.Name = "ListBoxData"
        Me.ListBoxData.Size = New System.Drawing.Size(492, 108)
        Me.ListBoxData.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 103)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "InTakes"
        '
        'ButtonGetApointments
        '
        Me.ButtonGetApointments.Location = New System.Drawing.Point(386, 8)
        Me.ButtonGetApointments.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonGetApointments.Name = "ButtonGetApointments"
        Me.ButtonGetApointments.Size = New System.Drawing.Size(116, 80)
        Me.ButtonGetApointments.TabIndex = 13
        Me.ButtonGetApointments.Text = "MyAppointments"
        Me.ButtonGetApointments.UseVisualStyleBackColor = True
        '
        'ButtonGetPrescriptions
        '
        Me.ButtonGetPrescriptions.Location = New System.Drawing.Point(263, 8)
        Me.ButtonGetPrescriptions.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonGetPrescriptions.Name = "ButtonGetPrescriptions"
        Me.ButtonGetPrescriptions.Size = New System.Drawing.Size(118, 80)
        Me.ButtonGetPrescriptions.TabIndex = 14
        Me.ButtonGetPrescriptions.Text = "MyPrescriptions"
        Me.ButtonGetPrescriptions.UseVisualStyleBackColor = True
        '
        'ButtonGetStock
        '
        Me.ButtonGetStock.Location = New System.Drawing.Point(140, 8)
        Me.ButtonGetStock.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonGetStock.Name = "ButtonGetStock"
        Me.ButtonGetStock.Size = New System.Drawing.Size(120, 80)
        Me.ButtonGetStock.TabIndex = 15
        Me.ButtonGetStock.Text = "MyStock"
        Me.ButtonGetStock.UseVisualStyleBackColor = True
        '
        'ButtonAdd
        '
        Me.ButtonAdd.Location = New System.Drawing.Point(12, 258)
        Me.ButtonAdd.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(124, 45)
        Me.ButtonAdd.TabIndex = 22
        Me.ButtonAdd.Text = "Add"
        Me.ButtonAdd.UseVisualStyleBackColor = True
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(265, 258)
        Me.ButtonRemove.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(116, 45)
        Me.ButtonRemove.TabIndex = 23
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = True
        '
        'ButtonGetInTakes
        '
        Me.ButtonGetInTakes.Location = New System.Drawing.Point(10, 8)
        Me.ButtonGetInTakes.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonGetInTakes.Name = "ButtonGetInTakes"
        Me.ButtonGetInTakes.Size = New System.Drawing.Size(125, 80)
        Me.ButtonGetInTakes.TabIndex = 24
        Me.ButtonGetInTakes.Text = "MyInTakes"
        Me.ButtonGetInTakes.UseVisualStyleBackColor = True
        '
        'ButtonEdit
        '
        Me.ButtonEdit.Location = New System.Drawing.Point(140, 258)
        Me.ButtonEdit.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonEdit.Name = "ButtonEdit"
        Me.ButtonEdit.Size = New System.Drawing.Size(120, 45)
        Me.ButtonEdit.TabIndex = 25
        Me.ButtonEdit.Text = "Edit"
        Me.ButtonEdit.UseVisualStyleBackColor = True
        '
        'ButtonStatistics
        '
        Me.ButtonStatistics.Location = New System.Drawing.Point(387, 258)
        Me.ButtonStatistics.Margin = New System.Windows.Forms.Padding(2)
        Me.ButtonStatistics.Name = "ButtonStatistics"
        Me.ButtonStatistics.Size = New System.Drawing.Size(116, 45)
        Me.ButtonStatistics.TabIndex = 26
        Me.ButtonStatistics.Text = "Statistics"
        Me.ButtonStatistics.UseVisualStyleBackColor = True
        '
        'ButtonSupervisees
        '
        Me.ButtonSupervisees.Location = New System.Drawing.Point(386, 98)
        Me.ButtonSupervisees.Name = "ButtonSupervisees"
        Me.ButtonSupervisees.Size = New System.Drawing.Size(116, 23)
        Me.ButtonSupervisees.TabIndex = 27
        Me.ButtonSupervisees.Text = "Get Supervisees"
        Me.ButtonSupervisees.UseVisualStyleBackColor = True
        '
        'ButtonDelUser
        '
        Me.ButtonDelUser.Location = New System.Drawing.Point(265, 98)
        Me.ButtonDelUser.Name = "ButtonDelUser"
        Me.ButtonDelUser.Size = New System.Drawing.Size(116, 23)
        Me.ButtonDelUser.TabIndex = 28
        Me.ButtonDelUser.Text = "Delete User"
        Me.ButtonDelUser.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(507, 316)
        Me.Controls.Add(Me.ButtonDelUser)
        Me.Controls.Add(Me.ButtonSupervisees)
        Me.Controls.Add(Me.ButtonStatistics)
        Me.Controls.Add(Me.ButtonEdit)
        Me.Controls.Add(Me.ButtonGetInTakes)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.ButtonAdd)
        Me.Controls.Add(Me.ButtonGetStock)
        Me.Controls.Add(Me.ButtonGetPrescriptions)
        Me.Controls.Add(Me.ButtonGetApointments)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListBoxData)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBoxData As ListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ButtonGetApointments As Button
    Friend WithEvents ButtonGetPrescriptions As Button
    Friend WithEvents ButtonGetStock As Button
    Friend WithEvents ButtonAdd As Button
    Friend WithEvents ButtonRemove As Button
    Friend WithEvents ButtonGetInTakes As Button
    Friend WithEvents ButtonEdit As Button
    Friend WithEvents ButtonStatistics As Button
    Friend WithEvents ButtonSupervisees As Button
    Friend WithEvents ButtonDelUser As Button
End Class
