﻿Imports System.Data.SqlClient

Public Class Supervisee_form

    Dim userId As Int16
    Dim CN As SqlConnection
    Dim CMD As SqlCommand

    Private Sub Supervisee_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        getSupervisees()
    End Sub

    Public Sub setUserId(id As Int16)
        userId = id
    End Sub

    Private Sub getSupervisees()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "Select * FROM dbo.getSupervisee(" + Convert.ToString(userId) + ")"

        CN.Open()

        Dim RDR As SqlDataReader

        RDR = CMD.ExecuteReader

        ListBoxSupervisees.Items.Clear()

        While RDR.Read
            Dim C As New User
            C.Id1 = RDR.Item("USER_ID")
            C.Name1 = RDR.Item("USER_NAME")
            C.Email1 = RDR.Item("USER_EMAIL")
            ListBoxSupervisees.Items.Add(C)
        End While


        CN.Close()
    End Sub

    Private Sub ListBoxSupervisees_DoubleClick(sender As Object, e As EventArgs) Handles ListBoxSupervisees.DoubleClick
        Dim C As New User
        C = CType(ListBoxSupervisees.Items.Item(ListBoxSupervisees.SelectedIndex), User)
        Dim oForm As New Form1
        oForm.setUserId(C.Id1)
        oForm.setNewFormAsSupervised()
        oForm.Show()
        Me.Close()
    End Sub
End Class