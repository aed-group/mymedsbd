﻿Public Class Prescription

    Private _prescriptionId As Int16
    Private _appointmentId As Int16
    Private _pharmacyName As String
    Private _buyDate As String

    Public Property PrescriptionId() As Short
        Get
            Return _prescriptionId
        End Get
        Set(value As Short)
            _prescriptionId = value
        End Set
    End Property

    Public Property AppointmentId() As Short
        Get
            Return _appointmentId
        End Get
        Set(value As Short)
            _appointmentId = value
        End Set
    End Property

    Public Property PharmacyName() As String
        Get
            Return _pharmacyName
        End Get
        Set(value As String)
            _pharmacyName = value
        End Set
    End Property

    Public Property BuyDate() As String
        Get
            Return _buyDate
        End Get
        Set(value As String)
            _buyDate = value
        End Set
    End Property

    Overrides Function ToString() As String
        Dim used As String = "Yes"
        If _buyDate = "Null" Then
            used = "No"
        End If
        Return "ID: " & _prescriptionId & ", Appointment ID: " & _appointmentId & ", Already Used: " & used
    End Function

End Class
