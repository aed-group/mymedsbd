﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Doctor_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ListBoxHC = New System.Windows.Forms.ListBox()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.txt_phone = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_email = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_area = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.info = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(74, 287)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(227, 17)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "Works in Following Health Centers:"
        '
        'ListBoxHC
        '
        Me.ListBoxHC.FormattingEnabled = True
        Me.ListBoxHC.ItemHeight = 16
        Me.ListBoxHC.Location = New System.Drawing.Point(77, 307)
        Me.ListBoxHC.Name = "ListBoxHC"
        Me.ListBoxHC.Size = New System.Drawing.Size(367, 196)
        Me.ListBoxHC.TabIndex = 44
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(77, 524)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(511, 60)
        Me.ButtonCancel.TabIndex = 43
        Me.ButtonCancel.Text = "Back"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'txt_phone
        '
        Me.txt_phone.Location = New System.Drawing.Point(238, 192)
        Me.txt_phone.Name = "txt_phone"
        Me.txt_phone.ReadOnly = True
        Me.txt_phone.Size = New System.Drawing.Size(350, 22)
        Me.txt_phone.TabIndex = 42
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(74, 192)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 17)
        Me.Label2.TabIndex = 41
        Me.Label2.Text = "Phone:"
        '
        'txt_email
        '
        Me.txt_email.Location = New System.Drawing.Point(238, 154)
        Me.txt_email.Name = "txt_email"
        Me.txt_email.ReadOnly = True
        Me.txt_email.Size = New System.Drawing.Size(350, 22)
        Me.txt_email.TabIndex = 40
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(74, 154)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 17)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Email:"
        '
        'txt_area
        '
        Me.txt_area.Location = New System.Drawing.Point(238, 113)
        Me.txt_area.Name = "txt_area"
        Me.txt_area.ReadOnly = True
        Me.txt_area.Size = New System.Drawing.Size(350, 22)
        Me.txt_area.TabIndex = 38
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(74, 113)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 17)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Expertise Area:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(235, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 17)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "Doctor Info"
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(238, 75)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.ReadOnly = True
        Me.txt_name.Size = New System.Drawing.Size(350, 22)
        Me.txt_name.TabIndex = 35
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(74, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 17)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Name:"
        '
        'info
        '
        Me.info.Location = New System.Drawing.Point(450, 307)
        Me.info.Name = "info"
        Me.info.Size = New System.Drawing.Size(126, 78)
        Me.info.TabIndex = 46
        Me.info.Text = "More Info"
        Me.info.UseVisualStyleBackColor = True
        '
        'Doctor_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(712, 596)
        Me.Controls.Add(Me.info)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ListBoxHC)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.txt_phone)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_email)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_area)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.Label4)
        Me.Name = "Doctor_Form"
        Me.Text = "Doctor_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label6 As Label
    Friend WithEvents ListBoxHC As ListBox
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents txt_phone As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_email As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_area As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents info As Button
End Class
