﻿Friend Class User
    Dim id As Int16
    Dim name As String
    Dim email As String

    Public Property Id1 As Short
        Get
            Return id
        End Get
        Set(value As Short)
            id = value
        End Set
    End Property

    Public Property Name1 As String
        Get
            Return name
        End Get
        Set(value As String)
            name = value
        End Set
    End Property

    Public Property Email1 As String
        Get
            Return email
        End Get
        Set(value As String)
            email = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return "ID: " + id.ToString + ", Name: " + name + ", E-mail: " + email
    End Function
End Class
