﻿Public Class Doctor
    Private _name As String
    Private _email As String
    Private _phone As String
    Private _area As String
    Private _id As Integer

    Public Property DName As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    Public Property DEmail As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Public Property DPhone As String
        Get
            Return _phone
        End Get
        Set(value As String)
            _phone = value
        End Set
    End Property

    Public Property DArea As String
        Get
            Return _area
        End Get
        Set(value As String)
            _area = value
        End Set
    End Property

    Public Property Did As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return _name + ", " + _area
    End Function
End Class
