﻿Imports System.Data.SqlClient

Public Class Prescription_Form
    Dim CN As SqlConnection
    Dim p As Prescription
    Dim CMD As SqlCommand
    Dim UserId As Integer
    Dim oForm As Form1
    Dim currentSelected As Integer


    Private Sub Prescription_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()

        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "Select * FROM dbo.getMeds_From_Prescription (" + Convert.ToString(p.PrescriptionId) + ")"

        Dim RDR As SqlDataReader
        CN.Open()
        RDR = CMD.ExecuteReader

        ListBoxMeds.Items.Clear()

        While RDR.Read
            Dim M As New Medicine
            M.Name = RDR.Item("MEDICINE_NAME")
            M.Unique_name = RDR.Item("MEDICINE_UNIQUE_NAME")
            M.Price = RDR.Item("PRICE")
            M.Formula = RDR.Item("MEDICINE_FORMULA")
            M.Validity = RDR.Item("VALIDITY")
            ListBoxMeds.Items.Add(M)
        End While
        CN.Close()
        currentSelected = 0
        showPres()
    End Sub

    Friend Sub setObj(pres As Prescription)
        p = pres
    End Sub

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Friend Sub setForm(f As Form1)
        oForm = f
    End Sub

    Sub showPres()
        txt_pharmacie.Text = p.PharmacyName
        txt_date.Text = p.BuyDate
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            Remove()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ''ir para farmacia
        Dim aForm As New Pharmacie_Form
        aForm.setPharmacie_Name(p.PharmacyName)
        aForm.Show()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "Select * FROM dbo.getAppointment_from_Prescription ('" + Convert.ToString(p.PrescriptionId) + "')"
        Dim RDR As SqlDataReader
        CN.Open()
        RDR = CMD.ExecuteReader

        Dim A As New Appointment
        While RDR.Read
            A.AppointmentId = RDR.Item("APPOINTMENT_ID")
            A.DoctorId = RDR.Item("DOCTOR_ID")
            A.DoctorName = RDR.Item("DOCTOR_NAME")
            A.HealthCenterName = RDR.Item("HEALTH_CENTER_NAME")
            A.AppointmentArea = RDR.Item("APPOINTMENT_AREA")
            A.AppointmentDate = RDR.Item("APPOINTMENT_DATE")
        End While
        CN.Close()

        Dim aForm As New Appointment_Form
        aForm.setObj(A)
        aForm.Show()
    End Sub

    Private Sub info_Click(sender As Object, e As EventArgs)
        Dim M As New Medicine
        M = CType(ListBoxMeds.Items.Item(currentSelected), Medicine)
        'Dim oForm As New Doctor_Form
        ''oForm.setDoc_ID(D.Did)
        'oForm.Show()
        'Me.Close()
        'open med specifications
    End Sub

    Sub Remove()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "EXEC dbo.spRemoveUserPrescriptions " + Convert.ToString(UserId) + ", " + p.PrescriptionId.ToString

        MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As String
        RDR = CMD.ExecuteScalar

        CN.Close()

        oForm.Refresh()

        Me.Close()

    End Sub
End Class