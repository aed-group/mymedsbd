﻿Public Class Medicine
    Private _unique_name As String
    Private _name As String
    Private _price As Double
    Private _formula As String
    Private _validity As Integer


    Property Validity() As Integer
        Get
            Return _validity
        End Get
        Set(ByVal value As Integer)
            _validity = value
        End Set
    End Property

    Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Property Price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property

    Property Formula() As String
        Get
            Return _formula
        End Get
        Set(ByVal value As String)
            _formula = value
        End Set
    End Property

    Property Unique_name() As String
        Get
            Unique_name = _unique_name
        End Get
        Set(ByVal value As String)
            _unique_name = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return "Medicine: " & _name & ",    Price: " & Math.Round(_price, 2) & ", Formula: " & _formula
    End Function

End Class
