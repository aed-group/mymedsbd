﻿Public Class Appointment

    Private _appointmentId As Int16
    Private _doctorName As String
    Private _doctorId As Integer
    Private _healthCenterName As String
    Private _appointmentArea As String
    Private _appointmentDate As String

    Public Property AppointmentId As Short
        Get
            Return _appointmentId
        End Get
        Set(value As Short)
            _appointmentId = value
        End Set
    End Property

    Public Property DoctorName As String
        Get
            Return _doctorName
        End Get
        Set(value As String)
            _doctorName = value
        End Set
    End Property

    Public Property DoctorId As Integer
        Get
            Return _doctorId
        End Get
        Set(value As Integer)
            _doctorId = value
        End Set
    End Property

    Public Property HealthCenterName As String
        Get
            Return _healthCenterName
        End Get
        Set(value As String)
            _healthCenterName = value
        End Set
    End Property

    Public Property AppointmentArea As String
        Get
            Return _appointmentArea
        End Get
        Set(value As String)
            _appointmentArea = value
        End Set
    End Property

    Public Property AppointmentDate As String
        Get
            Return _appointmentDate
        End Get
        Set(value As String)
            _appointmentDate = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return "Date: " + _appointmentDate + ", Area: " + _appointmentArea
    End Function

End Class
