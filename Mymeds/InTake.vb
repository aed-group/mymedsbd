﻿Public Class InTake
    Private _med As String
    Private _date As String
    Private _qnty As Int16



    Property Med() As String
        Get
            Return _med
        End Get
        Set(ByVal value As String)
            _med = value
        End Set
    End Property

    Property TDate() As String
        Get
            Return _date
        End Get
        Set(ByVal value As String)
            _date = value
        End Set
    End Property

    Property Qnty() As String
        Get
            Return _qnty
        End Get
        Set(ByVal value As String)
            _qnty = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return "Medicine: " + _med & ", Take at: " & _date & ", " & _qnty & " pills"
    End Function

End Class
