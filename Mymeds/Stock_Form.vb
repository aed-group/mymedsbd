﻿Imports System.Data.SqlClient

Public Class Stock_Form
    Dim CN As SqlConnection
    Dim st As Stock
    Dim CMD As SqlCommand
    Dim UserId As Integer
    Dim oForm As Form1

    Private Sub Stock_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        showStock()
    End Sub

    Friend Sub setObj(stock As Stock)
        st = stock
    End Sub

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Friend Sub setForm(f As Form1)
        oForm = f
    End Sub

    Sub showStock()
        txt_Name.Text = st.Name
        txt_unq_name.Text = st.Unique_name
        txt_formula.Text = st.Formula
        txt_validity.Text = st.Validity
        txt_price.Text = st.Price
        txt_expiration.Text = st.ExprDate
        txt_stock.Text = st.StockQuantity
    End Sub

    Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            Remove()
        End If
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub txt_stock_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_stock.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click

        If txt_expiration.Text.Contains("*") Or txt_expiration.Text.Contains("=") Or txt_stock.Text.Contains("*") Or txt_stock.Text.Contains("=") Then
            MsgBox("Invalid Input!")
            txt_expiration.Text = st.ExprDate
            txt_stock.Text = st.StockQuantity
            Return
        End If

        CMD = New SqlCommand
        CMD.Connection = CN
        Dim d(3) As String
        Dim finalSTR As String

        CN.Open()

        'Try

        '    d = Split(st.ExprDate, "/")
        '    finalSTR = d(2) + "-" + d(1) + "-" + d(0)

        '    CMD.CommandText = "EXEC dbo.spUpDateStock " & UserId & ", '" & st.Unique_name & "', '" & finalSTR & "', '" & Convert.ToInt16(txt_stock.Text) & "'"
        '    MsgBox(CMD.CommandText)

        '    Dim rows As Integer
        '    rows = CMD.ExecuteNonQuery
        '    If rows > 0 Then
        '        MsgBox("Updated successfully")
        '    Else
        '        MsgBox("Failed to Update")
        '    End If

        'Catch

        '    d = Split(st.ExprDate, "/")
        '    finalSTR = d(2) + "-" + d(0) + "-" + d(1)

        '    CMD.CommandText = "EXEC dbo.spUpDateStock " & UserId & ", '" & st.Unique_name & "', '" & finalSTR & "', '" & Convert.ToInt16(txt_stock.Text) & "'"
        '    MsgBox(CMD.CommandText)

        '    Dim rows As Integer
        '    rows = CMD.ExecuteNonQuery
        '    If rows > 0 Then
        '        MsgBox("Updated successfully")
        '    Else
        '        MsgBox("Failed to Update")
        '    End If

        'End Try

        CMD.CommandText = "EXEC dbo.spUpDateStock " & UserId & ", '" & st.Unique_name & "', '" & st.ExprDate & "', '" & Convert.ToInt16(txt_stock.Text) & "'"
        'MsgBox(CMD.CommandText)

        Dim rows As Integer
        rows = CMD.ExecuteNonQuery
        If rows > 0 Then
            MsgBox("Updated successfully")
        Else
            MsgBox("Failed to Update")
        End If


        If CN.State = ConnectionState.Open Then
            CN.Close()
        End If

        ''call refresh
        oForm.Refresh()
        Me.Close()
    End Sub

    Private Sub Remove()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "EXEC dbo.spRemoveUserStock " + Convert.ToString(UserId)

        CMD.CommandText += ", '" + st.Unique_name + "'"

        'MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As String
        RDR = CMD.ExecuteScalar

        CN.Close()

        oForm.LoadData()

        Me.Close()
    End Sub
End Class