﻿Imports System.Data.SqlClient

Public Class Appointment_Form
    Dim CN As SqlConnection
    Dim ap As Appointment
    Dim CMD As SqlCommand
    Dim UserId As Integer
    Dim oForm As Form1

    Private Sub Appointment_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        showApp()
    End Sub

    Friend Sub setObj(appointemnt As Appointment)
        ap = appointemnt
    End Sub

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Friend Sub setForm(f As Form1)
        oForm = f
    End Sub

    Sub showApp()
        txt_Area.Text = ap.AppointmentArea
        txt_date.Text = ap.AppointmentDate
        txt_hc.Text = ap.HealthCenterName
        txt_dn.Text = ap.DoctorName
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            Remove()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim hForm As New Health_Center_Form
        hForm.setHeath_Center_Name(ap.HealthCenterName)
        hForm.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim dForm As New Doctor_Form
        dForm.setDoc_ID(ap.DoctorId)
        dForm.Show()
    End Sub

    Sub Remove()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "EXEC dbo.spRemoveUserAppointment " + Convert.ToString(UserId)

        CMD.CommandText += ", " + Convert.ToString(ap.AppointmentId) + ""

        MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As String
        RDR = CMD.ExecuteScalar

        CN.Close()

        oForm.Refresh()

        Me.Close()

    End Sub
End Class