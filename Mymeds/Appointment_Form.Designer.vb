﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Appointment_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_Area = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_date = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_hc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_dn = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txt_Area
        '
        Me.txt_Area.Location = New System.Drawing.Point(278, 150)
        Me.txt_Area.Name = "txt_Area"
        Me.txt_Area.ReadOnly = True
        Me.txt_Area.Size = New System.Drawing.Size(350, 22)
        Me.txt_Area.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(114, 150)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 17)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Area:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(255, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Appointment Info"
        '
        'txt_date
        '
        Me.txt_date.Location = New System.Drawing.Point(278, 188)
        Me.txt_date.Name = "txt_date"
        Me.txt_date.ReadOnly = True
        Me.txt_date.Size = New System.Drawing.Size(350, 22)
        Me.txt_date.TabIndex = 19
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(114, 188)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 17)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Date:"
        '
        'txt_hc
        '
        Me.txt_hc.Location = New System.Drawing.Point(278, 237)
        Me.txt_hc.Name = "txt_hc"
        Me.txt_hc.ReadOnly = True
        Me.txt_hc.Size = New System.Drawing.Size(350, 22)
        Me.txt_hc.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(114, 237)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 17)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Health Center:"
        '
        'txt_dn
        '
        Me.txt_dn.Location = New System.Drawing.Point(278, 356)
        Me.txt_dn.Name = "txt_dn"
        Me.txt_dn.ReadOnly = True
        Me.txt_dn.Size = New System.Drawing.Size(350, 22)
        Me.txt_dn.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(114, 356)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 17)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Doctor Name:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(278, 265)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(234, 33)
        Me.Button1.TabIndex = 24
        Me.Button1.Text = "More Info"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(278, 384)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(234, 33)
        Me.Button2.TabIndex = 25
        Me.Button2.Text = "More Info"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(485, 487)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(143, 60)
        Me.ButtonRemove.TabIndex = 26
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(117, 487)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(143, 60)
        Me.ButtonCancel.TabIndex = 27
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'Appointment_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 598)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txt_dn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_hc)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_date)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_Area)
        Me.Controls.Add(Me.Label4)
        Me.Name = "Appointment_Form"
        Me.Text = "Appointment_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_Area As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_date As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_hc As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_dn As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents ButtonRemove As Button
    Friend WithEvents ButtonCancel As Button
End Class
