﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InTake_Add_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBoxMedSearch = New System.Windows.Forms.TextBox()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.txt_take_qnty = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_hour = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ListBoxMedicines = New System.Windows.Forms.ListBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_formula = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_unq_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_expiration = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TextBoxMedSearch
        '
        Me.TextBoxMedSearch.Location = New System.Drawing.Point(25, 26)
        Me.TextBoxMedSearch.Margin = New System.Windows.Forms.Padding(4)
        Me.TextBoxMedSearch.Name = "TextBoxMedSearch"
        Me.TextBoxMedSearch.Size = New System.Drawing.Size(373, 22)
        Me.TextBoxMedSearch.TabIndex = 82
        '
        'ButtonAdd
        '
        Me.ButtonAdd.Location = New System.Drawing.Point(741, 413)
        Me.ButtonAdd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(211, 74)
        Me.ButtonAdd.TabIndex = 79
        Me.ButtonAdd.Text = "Add"
        Me.ButtonAdd.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(25, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 17)
        Me.Label10.TabIndex = 78
        Me.Label10.Text = "My Medicines:"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(440, 413)
        Me.ButtonCancel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(211, 74)
        Me.ButtonCancel.TabIndex = 77
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'txt_take_qnty
        '
        Me.txt_take_qnty.Location = New System.Drawing.Point(601, 313)
        Me.txt_take_qnty.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_take_qnty.Name = "txt_take_qnty"
        Me.txt_take_qnty.Size = New System.Drawing.Size(351, 22)
        Me.txt_take_qnty.TabIndex = 76
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(437, 313)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 17)
        Me.Label1.TabIndex = 75
        Me.Label1.Text = "Take Quantity:"
        '
        'txt_hour
        '
        Me.txt_hour.Location = New System.Drawing.Point(601, 272)
        Me.txt_hour.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_hour.Name = "txt_hour"
        Me.txt_hour.Size = New System.Drawing.Size(351, 22)
        Me.txt_hour.TabIndex = 74
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(437, 272)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 17)
        Me.Label9.TabIndex = 73
        Me.Label9.Text = "Take hour:"
        '
        'ListBoxMedicines
        '
        Me.ListBoxMedicines.FormattingEnabled = True
        Me.ListBoxMedicines.ItemHeight = 16
        Me.ListBoxMedicines.Location = New System.Drawing.Point(25, 77)
        Me.ListBoxMedicines.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.ListBoxMedicines.Name = "ListBoxMedicines"
        Me.ListBoxMedicines.Size = New System.Drawing.Size(373, 404)
        Me.ListBoxMedicines.TabIndex = 72
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(600, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 17)
        Me.Label7.TabIndex = 67
        Me.Label7.Text = "Medicine Info"
        '
        'txt_formula
        '
        Me.txt_formula.Location = New System.Drawing.Point(602, 169)
        Me.txt_formula.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_formula.Name = "txt_formula"
        Me.txt_formula.ReadOnly = True
        Me.txt_formula.Size = New System.Drawing.Size(351, 22)
        Me.txt_formula.TabIndex = 66
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(438, 169)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 17)
        Me.Label6.TabIndex = 65
        Me.Label6.Text = "Medicine Formula:"
        '
        'txt_unq_name
        '
        Me.txt_unq_name.Location = New System.Drawing.Point(602, 117)
        Me.txt_unq_name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_unq_name.Name = "txt_unq_name"
        Me.txt_unq_name.ReadOnly = True
        Me.txt_unq_name.Size = New System.Drawing.Size(351, 22)
        Me.txt_unq_name.TabIndex = 64
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(438, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(158, 17)
        Me.Label3.TabIndex = 63
        Me.Label3.Text = "Medicine Unique Name:"
        '
        'txt_Name
        '
        Me.txt_Name.Location = New System.Drawing.Point(602, 69)
        Me.txt_Name.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_Name.Name = "txt_Name"
        Me.txt_Name.ReadOnly = True
        Me.txt_Name.Size = New System.Drawing.Size(351, 22)
        Me.txt_Name.TabIndex = 62
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(438, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 17)
        Me.Label4.TabIndex = 61
        Me.Label4.Text = "Medicine Name:"
        '
        'txt_expiration
        '
        Me.txt_expiration.Location = New System.Drawing.Point(601, 214)
        Me.txt_expiration.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txt_expiration.Name = "txt_expiration"
        Me.txt_expiration.ReadOnly = True
        Me.txt_expiration.Size = New System.Drawing.Size(351, 22)
        Me.txt_expiration.TabIndex = 60
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(437, 219)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 17)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "Expiration Date:"
        '
        'InTake_Add_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(978, 519)
        Me.Controls.Add(Me.TextBoxMedSearch)
        Me.Controls.Add(Me.ButtonAdd)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.txt_take_qnty)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_hour)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.ListBoxMedicines)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_formula)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_unq_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Name)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_expiration)
        Me.Controls.Add(Me.Label2)
        Me.Name = "InTake_Add_Form"
        Me.Text = "InTake_Add_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBoxMedSearch As TextBox
    Friend WithEvents ButtonAdd As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents txt_take_qnty As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_hour As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents ListBoxMedicines As ListBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_formula As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_unq_name As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_expiration As TextBox
    Friend WithEvents Label2 As Label
End Class
