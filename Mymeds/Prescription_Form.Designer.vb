﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Prescription_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txt_pharmacie = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_date = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ListBoxMeds = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(12, 527)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(143, 60)
        Me.ButtonCancel.TabIndex = 40
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(593, 527)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(143, 60)
        Me.ButtonRemove.TabIndex = 39
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(291, 238)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(234, 33)
        Me.Button1.TabIndex = 37
        Me.Button1.Text = "More Info"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txt_pharmacie
        '
        Me.txt_pharmacie.Location = New System.Drawing.Point(291, 210)
        Me.txt_pharmacie.Name = "txt_pharmacie"
        Me.txt_pharmacie.ReadOnly = True
        Me.txt_pharmacie.Size = New System.Drawing.Size(350, 22)
        Me.txt_pharmacie.TabIndex = 34
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(127, 210)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 17)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Buy Pharmacy:"
        '
        'txt_date
        '
        Me.txt_date.Location = New System.Drawing.Point(291, 161)
        Me.txt_date.Name = "txt_date"
        Me.txt_date.ReadOnly = True
        Me.txt_date.Size = New System.Drawing.Size(350, 22)
        Me.txt_date.TabIndex = 32
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(127, 161)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 17)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Buy Date:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(258, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(110, 17)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Prescription Info"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(127, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 17)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "Appointment:"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(291, 117)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(234, 23)
        Me.Button3.TabIndex = 41
        Me.Button3.Text = "Appointment Info"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ListBoxMeds
        '
        Me.ListBoxMeds.FormattingEnabled = True
        Me.ListBoxMeds.ItemHeight = 16
        Me.ListBoxMeds.Location = New System.Drawing.Point(12, 318)
        Me.ListBoxMeds.Name = "ListBoxMeds"
        Me.ListBoxMeds.Size = New System.Drawing.Size(724, 180)
        Me.ListBoxMeds.TabIndex = 42
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 288)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 17)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Medicines:"
        '
        'Prescription_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(748, 599)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ListBoxMeds)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txt_pharmacie)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_date)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Name = "Prescription_Form"
        Me.Text = "Prescription_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonCancel As Button
    Friend WithEvents ButtonRemove As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents txt_pharmacie As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_date As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents ListBoxMeds As ListBox
    Friend WithEvents Label3 As Label
End Class
