﻿Public Class Stock
    Private _unique_name As String
    Private _name As String
    Private _price As Double
    Private _formula As String
    Private _validity As Integer
    Private _stockQuantity As Int16
    Private _exprDate As String

    Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    Property Price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = Math.Round(value, 2)
        End Set
    End Property

    Property Formula() As String
        Get
            Return _formula
        End Get
        Set(ByVal value As String)
            _formula = value
        End Set
    End Property

    Property Unique_name() As String
        Get
            Unique_name = _unique_name
        End Get
        Set(ByVal value As String)
            _unique_name = value
        End Set
    End Property

    Public Property Validity As Integer
        Get
            Return _validity
        End Get
        Set(value As Integer)
            _validity = value
        End Set
    End Property

    Public Property ExprDate As String
        Get
            Return _exprDate
        End Get
        Set(value As String)
            _exprDate = value
        End Set
    End Property

    Public Property StockQuantity As Short
        Get
            Return _stockQuantity
        End Get
        Set(value As Short)
            _stockQuantity = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return "Name: " & _name & ", Expiration: " & _exprDate & ", Quantity: " & _stockQuantity
    End Function

End Class
