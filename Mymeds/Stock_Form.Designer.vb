﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Stock_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_formula = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_unq_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_expiration = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_validity = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_price = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_stock = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(483, 477)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(143, 60)
        Me.ButtonRemove.TabIndex = 29
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = True
        '
        'ButtonSave
        '
        Me.ButtonSave.Location = New System.Drawing.Point(299, 477)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(143, 60)
        Me.ButtonSave.TabIndex = 28
        Me.ButtonSave.Text = "Save"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(115, 477)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(143, 60)
        Me.ButtonCancel.TabIndex = 27
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(273, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 17)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Medicine Info"
        '
        'txt_formula
        '
        Me.txt_formula.Location = New System.Drawing.Point(276, 200)
        Me.txt_formula.Name = "txt_formula"
        Me.txt_formula.ReadOnly = True
        Me.txt_formula.Size = New System.Drawing.Size(350, 22)
        Me.txt_formula.TabIndex = 25
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(112, 200)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 17)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Medicine Formula:"
        '
        'txt_unq_name
        '
        Me.txt_unq_name.Location = New System.Drawing.Point(276, 148)
        Me.txt_unq_name.Name = "txt_unq_name"
        Me.txt_unq_name.ReadOnly = True
        Me.txt_unq_name.Size = New System.Drawing.Size(350, 22)
        Me.txt_unq_name.TabIndex = 23
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(112, 148)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(158, 17)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Medicine Unique Name:"
        '
        'txt_Name
        '
        Me.txt_Name.Location = New System.Drawing.Point(276, 101)
        Me.txt_Name.Name = "txt_Name"
        Me.txt_Name.ReadOnly = True
        Me.txt_Name.Size = New System.Drawing.Size(350, 22)
        Me.txt_Name.TabIndex = 21
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(112, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 17)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Medicine Name:"
        '
        'txt_expiration
        '
        Me.txt_expiration.Location = New System.Drawing.Point(276, 336)
        Me.txt_expiration.Name = "txt_expiration"
        Me.txt_expiration.ReadOnly = True
        Me.txt_expiration.Size = New System.Drawing.Size(350, 22)
        Me.txt_expiration.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(112, 336)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 17)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Expiration Date:"
        '
        'txt_validity
        '
        Me.txt_validity.Location = New System.Drawing.Point(276, 241)
        Me.txt_validity.Name = "txt_validity"
        Me.txt_validity.ReadOnly = True
        Me.txt_validity.Size = New System.Drawing.Size(350, 22)
        Me.txt_validity.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(112, 241)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(159, 17)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Medicine Validity(Days):"
        '
        'txt_price
        '
        Me.txt_price.Location = New System.Drawing.Point(276, 286)
        Me.txt_price.Name = "txt_price"
        Me.txt_price.ReadOnly = True
        Me.txt_price.Size = New System.Drawing.Size(350, 22)
        Me.txt_price.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(112, 286)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 17)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Medicine Price:"
        '
        'txt_stock
        '
        Me.txt_stock.Location = New System.Drawing.Point(276, 381)
        Me.txt_stock.Name = "txt_stock"
        Me.txt_stock.Size = New System.Drawing.Size(350, 22)
        Me.txt_stock.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(112, 381)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 17)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Current Stock:"
        '
        'Stock_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 594)
        Me.Controls.Add(Me.txt_stock)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_price)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txt_validity)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_formula)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_unq_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Name)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_expiration)
        Me.Controls.Add(Me.Label2)
        Me.Name = "Stock_Form"
        Me.Text = "Stock_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ButtonRemove As Button
    Friend WithEvents ButtonSave As Button
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_formula As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_unq_name As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_expiration As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_validity As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_price As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_stock As TextBox
    Friend WithEvents Label1 As Label
End Class
