﻿Imports System.Data.SqlClient

Public Class Doctor_Form
    Dim CN As SqlConnection
    Dim CMD As SqlCommand
    Dim myId As String
    Dim currentSelected As Integer

    Private Sub Doctor_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "Select * FROM dbo.udfHealth_From_Doc (" + myId + ")"

        Dim RDR As SqlDataReader
        CN.Open()
        RDR = CMD.ExecuteReader

        ListBoxHC.Items.Clear()

        While RDR.Read
            Dim Hc As New Health_Center
            Hc.HName = RDR.Item("HEALTH_CENTER_NAME")
            ListBoxHC.Items.Add(Hc)
        End While
        CN.Close()
        currentSelected = 0

        '------------------------------------------------------
        CMD.CommandText = "Select * FROM dbo.udfDoc_info (" + myId + ")"
        CN.Open()
        RDR = CMD.ExecuteReader
        Dim D As New Doctor
        While RDR.Read
            D.DName = RDR.Item("DOCTOR_NAME")
            D.DEmail = RDR.Item("DOCTOR_EMAIL")
            D.DArea = RDR.Item("EXPERTISE_AREA")
            D.DPhone = RDR.Item("DOCTOR_PHONE")
        End While
        CN.Close()
        ShowD(D)
    End Sub

    Sub ShowD(D As Doctor)
        txt_name.Text = D.DName
        txt_area.Text = D.DArea
        txt_email.Text = D.DEmail
        txt_phone.Text = D.DPhone
    End Sub

    Friend Sub setDoc_ID(id As Integer)
        myId = id
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub ListBoxHC_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxHC.SelectedIndexChanged
        If ListBoxHC.SelectedIndex > -1 Then
            currentSelected = ListBoxHC.SelectedIndex
        End If
    End Sub

    Private Sub info_Click(sender As Object, e As EventArgs) Handles info.Click
        Dim H As New Health_Center
        H = CType(ListBoxHC.Items.Item(currentSelected), Health_Center)
        Dim oForm As New Health_Center_Form
        oForm.setHeath_Center_Name(H.HName)
        oForm.Show()
        Me.Close()
    End Sub
End Class