﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Intake_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_intake_hour = New System.Windows.Forms.TextBox()
        Me.txt_intake_qnty = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_unq_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_formula = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.ButtonRemove = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(85, 276)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Take Hour:"
        '
        'txt_intake_hour
        '
        Me.txt_intake_hour.Location = New System.Drawing.Point(208, 276)
        Me.txt_intake_hour.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_intake_hour.Name = "txt_intake_hour"
        Me.txt_intake_hour.Size = New System.Drawing.Size(264, 20)
        Me.txt_intake_hour.TabIndex = 1
        '
        'txt_intake_qnty
        '
        Me.txt_intake_qnty.Location = New System.Drawing.Point(208, 314)
        Me.txt_intake_qnty.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_intake_qnty.Name = "txt_intake_qnty"
        Me.txt_intake_qnty.Size = New System.Drawing.Size(264, 20)
        Me.txt_intake_qnty.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(85, 314)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Take Quantity:"
        '
        'txt_unq_name
        '
        Me.txt_unq_name.Location = New System.Drawing.Point(208, 154)
        Me.txt_unq_name.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_unq_name.Name = "txt_unq_name"
        Me.txt_unq_name.ReadOnly = True
        Me.txt_unq_name.Size = New System.Drawing.Size(264, 20)
        Me.txt_unq_name.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(85, 154)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Medicine Unique Name:"
        '
        'txt_Name
        '
        Me.txt_Name.Location = New System.Drawing.Point(208, 115)
        Me.txt_Name.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_Name.Name = "txt_Name"
        Me.txt_Name.ReadOnly = True
        Me.txt_Name.Size = New System.Drawing.Size(264, 20)
        Me.txt_Name.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(85, 115)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Medicine Name:"
        '
        'txt_formula
        '
        Me.txt_formula.Location = New System.Drawing.Point(208, 196)
        Me.txt_formula.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_formula.Name = "txt_formula"
        Me.txt_formula.ReadOnly = True
        Me.txt_formula.Size = New System.Drawing.Size(264, 20)
        Me.txt_formula.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(85, 196)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Medicine Formula:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(206, 43)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Medicine Info"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(87, 384)
        Me.ButtonCancel.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(107, 49)
        Me.ButtonCancel.TabIndex = 13
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'ButtonSave
        '
        Me.ButtonSave.Location = New System.Drawing.Point(225, 384)
        Me.ButtonSave.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(107, 49)
        Me.ButtonSave.TabIndex = 14
        Me.ButtonSave.Text = "Save"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'ButtonRemove
        '
        Me.ButtonRemove.Location = New System.Drawing.Point(363, 384)
        Me.ButtonRemove.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonRemove.Name = "ButtonRemove"
        Me.ButtonRemove.Size = New System.Drawing.Size(107, 49)
        Me.ButtonRemove.TabIndex = 15
        Me.ButtonRemove.Text = "Remove"
        Me.ButtonRemove.UseVisualStyleBackColor = True
        '
        'Intake_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(545, 481)
        Me.Controls.Add(Me.ButtonRemove)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_formula)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_unq_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Name)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_intake_qnty)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_intake_hour)
        Me.Controls.Add(Me.Label1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Intake_Form"
        Me.Text = "Intake_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents txt_intake_hour As TextBox
    Friend WithEvents txt_intake_qnty As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_unq_name As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_formula As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents ButtonSave As Button
    Friend WithEvents ButtonRemove As Button
End Class
