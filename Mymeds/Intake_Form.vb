﻿Imports System.Data.SqlClient

Public Class Intake_Form
    Dim CN As SqlConnection
    Dim it As InTake
    Dim CMD As SqlCommand
    Dim UserId As Integer
    Dim oForm As Form1


    Private Sub Intake_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        Dim name As String
        Dim formula As String
        Dim comand As New SqlCommand("SELECT * FROM dbo.udfMed_MoreInfo ( '" + it.Med + "' )", CN)

        Dim RDR As SqlDataReader
        CN.Open()

        RDR = comand.ExecuteReader
        While RDR.Read
            name = RDR.Item("MEDICINE_NAME")
            formula = RDR.Item("MEDICINE_FORMULA")
        End While
        CN.Close()
        showIntake(name, formula)
    End Sub

    Friend Sub setObj(intake As InTake)
        it = intake
    End Sub

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Friend Sub setForm(f As Form1)
        oForm = f
    End Sub

    Sub showIntake(name As String, formula As String)
        txt_Name.Text = name
        txt_unq_name.Text = it.Med
        txt_formula.Text = formula
        txt_intake_hour.Text = it.TDate
        txt_intake_qnty.Text = it.Qnty
    End Sub

    Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            Remove()
        End If
    End Sub

    Private Sub Remove()
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "EXEC dbo.spRemoveUserTakes " & UserId & ", '" & it.Med & "', '" & it.TDate & "'"

        CN.Open()
        Dim rows As Integer
        rows = CMD.ExecuteNonQuery
        If rows > 0 Then
            MsgBox("Deleted successfully")
        Else
            MsgBox("Failed to Delete")
        End If
        If CN.State = ConnectionState.Open Then
            CN.Close()
        End If

        ''call refresh
        oForm.Refresh()
        Me.Close()
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click

        If txt_intake_hour.Text.Contains("*") Or txt_intake_hour.Text.Contains("=") Or txt_intake_qnty.Text.Contains("*") Or txt_intake_qnty.Text.Contains("=") Then
            MsgBox("Invalid Input!")
            txt_intake_hour.Text = it.TDate
            txt_intake_qnty.Text = it.Qnty
            Return
        End If

        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "EXEC dbo.upDateInTake " & UserId & ", '" & it.Med & "', '" & it.TDate & "', '" & txt_intake_hour.Text & "', '" & Convert.ToInt16(txt_intake_qnty.Text) & "'"
        CN.Open()
        Dim rows As Integer
        rows = CMD.ExecuteNonQuery
        If rows > 0 Then
            MsgBox("Updated successfully")
        Else
            MsgBox("Failed to Update")
        End If
        If CN.State = ConnectionState.Open Then
            CN.Close()
        End If
        ''call refresh
        oForm.Refresh()
        Me.Close()

    End Sub


    Private Sub txt_intake_qnty_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_intake_qnty.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        ''call refresh
        Me.Close()

    End Sub
End Class