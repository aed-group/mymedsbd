﻿Imports System.Data.SqlClient

Public Class Health_Center_Form
    Dim CN As SqlConnection
    Dim CMD As SqlCommand
    Dim myName As String
    Dim currentSelected As Integer
    'Dim oForm As Form1

    Private Sub Health_Center_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "Select * FROM dbo.udfDoc_From_HealthC ('" + myName + "')"

        Dim RDR As SqlDataReader
        CN.Open()
        RDR = CMD.ExecuteReader

        ListBoxDocs.Items.Clear()

        While RDR.Read
            Dim D As New Doctor
            D.DName = RDR.Item("DOCTOR_NAME")
            D.DEmail = RDR.Item("DOCTOR_EMAIL")
            D.DArea = RDR.Item("EXPERTISE_AREA")
            D.Did = RDR.Item("DOCTOR_ID")
            D.DPhone = RDR.Item("DOCTOR_PHONE")
            ListBoxDocs.Items.Add(D)
        End While
        CN.Close()
        currentSelected = 0

        '------------------------------------------------------
        CMD.CommandText = "Select * FROM dbo.udfHealthcenter_info ('" + myName + "')"
        CN.Open()
        RDR = CMD.ExecuteReader
        Dim H As New Health_Center
        While RDR.Read
            H.HAddress = RDR.Item("HEALTH_CENTER_ADDRESS")
            H.HOpen = RDR.Item("OPENING_TIME")
            H.HClose = RDR.Item("CLOSING_TIME")
        End While
        CN.Close()
        showhC(H)
    End Sub

    Sub showhC(H As Health_Center)
        txt_name.Text = myName
        txt_address.Text = H.HAddress
        txt_open.Text = H.HOpen
        txt_close.Text = H.HClose
    End Sub

    Friend Sub setHeath_Center_Name(name As String)
        myName = name
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub ListBoxDocs_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxDocs.SelectedIndexChanged
        If ListBoxDocs.SelectedIndex > -1 Then
            currentSelected = ListBoxDocs.SelectedIndex
        End If
    End Sub

    Private Sub info_Click(sender As Object, e As EventArgs) Handles info.Click
        Dim D As New Doctor
        D = CType(ListBoxDocs.Items.Item(currentSelected), Doctor)
        Dim oForm As New Doctor_Form
        oForm.setDoc_ID(D.Did)
        oForm.Show()
        Me.Close()
    End Sub
End Class