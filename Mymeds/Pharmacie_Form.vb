﻿Imports System.Data.SqlClient

Public Class Pharmacie_Form
    Dim CN As SqlConnection
    Dim CMD As SqlCommand
    Dim myName As String

    Private Sub Pharmacie_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        Dim RDR As SqlDataReader
        CMD.CommandText = "Select * FROM dbo.udfPharmacie_info ('" + myName + "')"
        CN.Open()
        RDR = CMD.ExecuteReader
        Dim P As New Pharmacie
        While RDR.Read
            P.Name1 = RDR.Item("PHARMACY_NAME")
            P.Address1 = RDR.Item("PHARMACY_ADDRESS")
            P.Op1 = RDR.Item("OPENING_TIME")
            P.Cl1 = RDR.Item("CLOSING_TIME")
        End While
        CN.Close()
        showPh(P)
    End Sub

    Sub showPh(P As Pharmacie)
        txt_name.Text = myName
        txt_address.Text = P.Address1
        txt_open.Text = P.Op1
        txt_close.Text = P.Cl1
    End Sub

    Friend Sub setPharmacie_Name(name As String)
        myName = name
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub
End Class