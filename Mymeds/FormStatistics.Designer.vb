﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormStatistics
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LabelTotalSpent = New System.Windows.Forms.Label()
        Me.LabelTotalSpentByMonth = New System.Windows.Forms.Label()
        Me.ComboBoxMonthSelection = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LabelFavouriteHC = New System.Windows.Forms.Label()
        Me.LabelFavouriteDoctor = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBoxPharmacyName = New System.Windows.Forms.TextBox()
        Me.ButtonSearch = New System.Windows.Forms.Button()
        Me.ListBoxPharmacies = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Spending Statistics"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(43, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Total Spent:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(43, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(134, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Total Spent By Month:"
        '
        'LabelTotalSpent
        '
        Me.LabelTotalSpent.AutoSize = True
        Me.LabelTotalSpent.Location = New System.Drawing.Point(339, 41)
        Me.LabelTotalSpent.Name = "LabelTotalSpent"
        Me.LabelTotalSpent.Size = New System.Drawing.Size(35, 13)
        Me.LabelTotalSpent.TabIndex = 3
        Me.LabelTotalSpent.Text = "NULL"
        '
        'LabelTotalSpentByMonth
        '
        Me.LabelTotalSpentByMonth.AutoSize = True
        Me.LabelTotalSpentByMonth.Location = New System.Drawing.Point(339, 68)
        Me.LabelTotalSpentByMonth.Name = "LabelTotalSpentByMonth"
        Me.LabelTotalSpentByMonth.Size = New System.Drawing.Size(35, 13)
        Me.LabelTotalSpentByMonth.TabIndex = 4
        Me.LabelTotalSpentByMonth.Text = "NULL"
        '
        'ComboBoxMonthSelection
        '
        Me.ComboBoxMonthSelection.FormattingEnabled = True
        Me.ComboBoxMonthSelection.Location = New System.Drawing.Point(194, 64)
        Me.ComboBoxMonthSelection.Name = "ComboBoxMonthSelection"
        Me.ComboBoxMonthSelection.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxMonthSelection.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 20)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Favourite Statistics"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(43, 130)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(106, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Favourite Doctor:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(43, 156)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(146, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Favourite Health Center:"
        '
        'LabelFavouriteHC
        '
        Me.LabelFavouriteHC.AutoSize = True
        Me.LabelFavouriteHC.Location = New System.Drawing.Point(339, 156)
        Me.LabelFavouriteHC.Name = "LabelFavouriteHC"
        Me.LabelFavouriteHC.Size = New System.Drawing.Size(35, 13)
        Me.LabelFavouriteHC.TabIndex = 10
        Me.LabelFavouriteHC.Text = "NULL"
        '
        'LabelFavouriteDoctor
        '
        Me.LabelFavouriteDoctor.AutoSize = True
        Me.LabelFavouriteDoctor.Location = New System.Drawing.Point(339, 130)
        Me.LabelFavouriteDoctor.Name = "LabelFavouriteDoctor"
        Me.LabelFavouriteDoctor.Size = New System.Drawing.Size(35, 13)
        Me.LabelFavouriteDoctor.TabIndex = 9
        Me.LabelFavouriteDoctor.Text = "NULL"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(8, 185)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(207, 20)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Search Pharmacies (by City)"
        '
        'TextBoxPharmacyName
        '
        Me.TextBoxPharmacyName.Location = New System.Drawing.Point(46, 219)
        Me.TextBoxPharmacyName.Name = "TextBoxPharmacyName"
        Me.TextBoxPharmacyName.Size = New System.Drawing.Size(269, 20)
        Me.TextBoxPharmacyName.TabIndex = 12
        '
        'ButtonSearch
        '
        Me.ButtonSearch.Location = New System.Drawing.Point(322, 219)
        Me.ButtonSearch.Name = "ButtonSearch"
        Me.ButtonSearch.Size = New System.Drawing.Size(77, 20)
        Me.ButtonSearch.TabIndex = 13
        Me.ButtonSearch.Text = "Search"
        Me.ButtonSearch.UseVisualStyleBackColor = True
        '
        'ListBoxPharmacies
        '
        Me.ListBoxPharmacies.FormattingEnabled = True
        Me.ListBoxPharmacies.Location = New System.Drawing.Point(46, 246)
        Me.ListBoxPharmacies.Name = "ListBoxPharmacies"
        Me.ListBoxPharmacies.Size = New System.Drawing.Size(353, 108)
        Me.ListBoxPharmacies.TabIndex = 14
        '
        'FormStatistics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(486, 367)
        Me.Controls.Add(Me.ListBoxPharmacies)
        Me.Controls.Add(Me.ButtonSearch)
        Me.Controls.Add(Me.TextBoxPharmacyName)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.LabelFavouriteHC)
        Me.Controls.Add(Me.LabelFavouriteDoctor)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBoxMonthSelection)
        Me.Controls.Add(Me.LabelTotalSpentByMonth)
        Me.Controls.Add(Me.LabelTotalSpent)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormStatistics"
        Me.Text = "FormStatistics"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents LabelTotalSpent As Label
    Friend WithEvents LabelTotalSpentByMonth As Label
    Friend WithEvents ComboBoxMonthSelection As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents LabelFavouriteHC As Label
    Friend WithEvents LabelFavouriteDoctor As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents TextBoxPharmacyName As TextBox
    Friend WithEvents ButtonSearch As Button
    Friend WithEvents ListBoxPharmacies As ListBox
End Class
