﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Health_Center_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_address = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_close = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_open = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.ListBoxDocs = New System.Windows.Forms.ListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.info = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txt_address
        '
        Me.txt_address.Location = New System.Drawing.Point(237, 130)
        Me.txt_address.Name = "txt_address"
        Me.txt_address.ReadOnly = True
        Me.txt_address.Size = New System.Drawing.Size(350, 22)
        Me.txt_address.TabIndex = 24
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(73, 130)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 17)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Address:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(234, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(122, 17)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Health Center Info"
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(237, 92)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.ReadOnly = True
        Me.txt_name.Size = New System.Drawing.Size(350, 22)
        Me.txt_name.TabIndex = 21
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(73, 92)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 17)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Name:"
        '
        'txt_close
        '
        Me.txt_close.Location = New System.Drawing.Point(237, 209)
        Me.txt_close.Name = "txt_close"
        Me.txt_close.ReadOnly = True
        Me.txt_close.Size = New System.Drawing.Size(350, 22)
        Me.txt_close.TabIndex = 28
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(73, 209)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 17)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Closing Time:"
        '
        'txt_open
        '
        Me.txt_open.Location = New System.Drawing.Point(237, 171)
        Me.txt_open.Name = "txt_open"
        Me.txt_open.ReadOnly = True
        Me.txt_open.Size = New System.Drawing.Size(350, 22)
        Me.txt_open.TabIndex = 26
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(73, 171)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(101, 17)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Opening Time:"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(76, 545)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(511, 60)
        Me.ButtonCancel.TabIndex = 29
        Me.ButtonCancel.Text = "Back"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'ListBoxDocs
        '
        Me.ListBoxDocs.FormattingEnabled = True
        Me.ListBoxDocs.ItemHeight = 16
        Me.ListBoxDocs.Location = New System.Drawing.Point(76, 324)
        Me.ListBoxDocs.Name = "ListBoxDocs"
        Me.ListBoxDocs.Size = New System.Drawing.Size(367, 196)
        Me.ListBoxDocs.TabIndex = 30
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(73, 304)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(147, 17)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Health Center Medics:"
        '
        'info
        '
        Me.info.Location = New System.Drawing.Point(449, 324)
        Me.info.Name = "info"
        Me.info.Size = New System.Drawing.Size(126, 78)
        Me.info.TabIndex = 47
        Me.info.Text = "More Info"
        Me.info.UseVisualStyleBackColor = True
        '
        'Health_Center_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(698, 617)
        Me.Controls.Add(Me.info)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.ListBoxDocs)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.txt_close)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_open)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_address)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.Label4)
        Me.Name = "Health_Center_Form"
        Me.Text = "Health_Center_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_address As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txt_name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_close As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_open As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents ListBoxDocs As ListBox
    Friend WithEvents Label6 As Label
    Friend WithEvents info As Button
End Class
