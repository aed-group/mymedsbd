﻿Public Class Pharmacie
    Private name As String
    Private address As String
    Private op As String
    Private cl As String

    Public Property Name1 As String
        Get
            Return name
        End Get
        Set(value As String)
            name = value
        End Set
    End Property

    Public Property Address1 As String
        Get
            Return address
        End Get
        Set(value As String)
            address = value
        End Set
    End Property

    Public Property Op1 As String
        Get
            Return op
        End Get
        Set(value As String)
            op = value
        End Set
    End Property

    Public Property Cl1 As String
        Get
            Return cl
        End Get
        Set(value As String)
            cl = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return "Pharmacy Name: " + name + ", Address: " + address + ", Opening time: " + op + ", Closing time: " + cl
    End Function
End Class
