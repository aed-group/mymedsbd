﻿Public Class Health_Center
    Private _name As String
    Private _address As String
    Private _open As String
    Private _close As String

    Public Property HName As String
        Get
            Return _name
        End Get
        Set(value As String)
            _name = value
        End Set
    End Property

    Public Property HAddress As String
        Get
            Return _address
        End Get
        Set(value As String)
            _address = value
        End Set
    End Property

    Public Property HOpen As String
        Get
            Return _open
        End Get
        Set(value As String)
            _open = value
        End Set
    End Property

    Public Property HClose As String
        Get
            Return _close
        End Get
        Set(value As String)
            _close = value
        End Set
    End Property

    Overrides Function ToString() As String
        Return _name
    End Function

End Class
