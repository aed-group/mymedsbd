﻿Imports System.Data.SqlClient

Public Class FormStatistics

    Dim UserId As Integer
    Dim CN As SqlConnection
    Dim CMD As SqlCommand

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Private Sub FormStatistics_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim monthDict As New Dictionary(Of String, String)()
        monthDict.Add("January", "1")
        monthDict.Add("February", "2")
        monthDict.Add("March", "3")
        monthDict.Add("April", "4")
        monthDict.Add("May", "5")
        monthDict.Add("June", "6")
        monthDict.Add("July", "7")
        monthDict.Add("August", "8")
        monthDict.Add("September", "9")
        monthDict.Add("October", "10")
        monthDict.Add("November", "11")
        monthDict.Add("December", "12")

        ComboBoxMonthSelection.DataSource = New BindingSource(monthDict, Nothing)
        ComboBoxMonthSelection.DisplayMember = "Key"
        ComboBoxMonthSelection.ValueMember = "Value"

        GetTotalSpent()
        GetTotalSpentMonth()
        GetFavouriteDoctor()
        GetFavouriteHC()

    End Sub

    Private Sub GetFavouriteHC()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        CMD.CommandText = "select * from dbo.getFavouriteHCByUser(" + Convert.ToString(UserId) + ")"

        CN.Open()

        Dim RDR As SqlDataReader
        RDR = CMD.ExecuteReader

        If RDR.Read Then
            If RDR.IsDBNull(RDR.GetOrdinal("HEALTH_CENTER_NAME")) Or RDR.IsDBNull(RDR.GetOrdinal("HC_COUNT")) Then
                CN.Close()
                Return
            End If
        End If

        LabelFavouriteHC.Text = RDR.Item("HEALTH_CENTER_NAME") + ": " + Convert.ToString(RDR.Item("HC_COUNT"))

        CN.Close()
    End Sub

    Private Sub GetFavouriteDoctor()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        CMD.CommandText = "select * from dbo.getFavouriteDoctorByUser(" + Convert.ToString(UserId) + ")"

        CN.Open()

        Dim RDR As SqlDataReader
        RDR = CMD.ExecuteReader

        If RDR.Read Then
            If RDR.IsDBNull(RDR.GetOrdinal("DOCTOR_NAME")) Or RDR.IsDBNull(RDR.GetOrdinal("APP_COUNT")) Then
                CN.Close()
                Return
            End If
        End If

        LabelFavouriteDoctor.Text = RDR.Item("DOCTOR_NAME") + ": " + Convert.ToString(RDR.Item("APP_COUNT"))


        CN.Close()
    End Sub

    Private Sub GetTotalSpentMonth()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        CMD.CommandText = "select * from dbo.getTotalSpentByUserMonthly(" + Convert.ToString(UserId) + ", " + ComboBoxMonthSelection.SelectedValue + ")"

        'MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As SqlDataReader
        RDR = CMD.ExecuteReader


        If RDR.Read Then
            If RDR.IsDBNull(RDR.GetOrdinal("TOTAL_SPENT")) Then
                LabelTotalSpentByMonth.Text = "0"
                CN.Close()
                Return
            End If
        End If

        LabelTotalSpentByMonth.Text = Math.Round(RDR.Item("TOTAL_SPENT"), 2)

        CN.Close()
    End Sub

    Private Sub GetTotalSpent()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        CMD.CommandText = "select * from dbo.getTotalSpentByUser(" + Convert.ToString(UserId) + ")"

        CN.Open()

        Dim RDR As SqlDataReader
        RDR = CMD.ExecuteReader

        If RDR.Read Then
            If RDR.IsDBNull(RDR.GetOrdinal("TOTAL_SPENT")) Then
                LabelTotalSpent.Text = "0"
                CN.Close()
                Return
            End If
        End If

        LabelTotalSpent.Text = Math.Round(RDR.Item("TOTAL_SPENT"), 2)

        CN.Close()
    End Sub

    Private Sub ComboBoxMonthSelection_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles ComboBoxMonthSelection.SelectionChangeCommitted
        GetTotalSpentMonth()
    End Sub

    Private Sub GetPharmacies()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "Select * FROM dbo.getPharmacies('" + TextBoxPharmacyName.Text + "')"

        CN.Open()

        Dim RDR As SqlDataReader
        RDR = CMD.ExecuteReader

        ListBoxPharmacies.Items.Clear()

        While RDR.Read
            Dim C As New Pharmacie
            C.Name1 = RDR.Item("PHARMACY_NAME")
            C.Address1 = RDR.Item("PHARMACY_ADDRESS")
            C.Op1 = RDR.Item("OPENING_TIME")
            C.Op1 = RDR.Item("CLOSING_TIME")
            ListBoxPharmacies.Items.Add(C)
        End While


        CN.Close()
    End Sub

    Private Sub ButtonSearch_Click(sender As Object, e As EventArgs) Handles ButtonSearch.Click
        GetPharmacies()
    End Sub

    Private Sub ListBoxPharmacies_DoubleClick(sender As Object, e As EventArgs) Handles ListBoxPharmacies.DoubleClick
        Dim Pharmacy_form As New Pharmacie_Form
        Dim C As Pharmacie
        If ListBoxPharmacies.SelectedIndex > -1 Then
            C = CType(ListBoxPharmacies.Items.Item(ListBoxPharmacies.SelectedIndex), Pharmacie)
            Pharmacy_form.setPharmacie_Name(C.Name1)
            Pharmacy_form.Show()
        End If
    End Sub
End Class