﻿Imports System.Data.SqlClient

Public Class Form1
    Dim CN As SqlConnection
    Dim CMD As SqlCommand
    Dim currentSelected As Integer
    Dim UserId As Integer
    Dim currentObjectType As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        firstPage.Hide()

        'MsgBox("On form1 User id: " + UserId.ToString())

        currentObjectType = "UserTakes"
        LoadData()

        'ShowInTakes()
    End Sub

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Sub LoadData()

        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        If currentObjectType.Equals("") Then
            currentObjectType = "UserTakes"
        End If

        CMD.CommandText = "Select * FROM dbo.get" + currentObjectType + "(" + Convert.ToString(UserId) + ")"

        CN.Open()

        Dim RDR As SqlDataReader

        RDR = CMD.ExecuteReader

        ListBoxData.Items.Clear()

        If (currentObjectType.Equals("UserTakes")) Then
            While RDR.Read
                Dim C As New InTake
                C.Med = RDR.Item("MEDICINE_U_NAME")
                C.TDate = RDR.Item("TAKE_HOUR")
                C.Qnty = RDR.Item("TAKE_QUANTITY")
                ListBoxData.Items.Add(C)
            End While
        ElseIf (currentObjectType.Equals("UserStock")) Then
            While RDR.Read
                Dim C As New Stock
                C.Unique_name = RDR.Item("MEDICINE_UNIQUE_NAME")
                C.Name = RDR.Item("MEDICINE_NAME")
                C.Price = RDR.Item("PRICE")
                C.Formula = RDR.Item("MEDICINE_FORMULA")
                C.StockQuantity = RDR.Item("QUANTITY")
                C.Validity = RDR.Item("VALIDITY")
                Dim d As DateTime
                d = RDR.Item("EXPIRATION_DATE")
                C.ExprDate = d.ToString("yyyy-MM-dd")
                ListBoxData.Items.Add(C)
            End While
        ElseIf (currentObjectType.Equals("UserPrescriptions")) Then
            While RDR.Read
                Dim C As New Prescription
                C.PrescriptionId = RDR.Item("PRESCRIPTION_ID")
                C.AppointmentId = RDR.Item("APPOINTMENT_ID")
                C.PharmacyName = RDR.Item("PHARMACY_NAME")
                C.BuyDate = RDR.Item("BUY_DATE")
                ListBoxData.Items.Add(C)
            End While
        ElseIf (currentObjectType.Equals("UserAppointments")) Then
            While RDR.Read
                Dim C As New Appointment
                C.AppointmentId = RDR.Item("APPOINTMENT_ID")
                C.DoctorName = RDR.Item("DOCTOR_NAME")
                C.DoctorId = RDR.Item("DOCTOR_ID")
                C.HealthCenterName = RDR.Item("HEALTH_CENTER_NAME")
                C.AppointmentArea = RDR.Item("APPOINTMENT_AREA")
                C.AppointmentDate = RDR.Item("APPOINTMENT_DATE")
                ListBoxData.Items.Add(C)
            End While
        End If

        CN.Close()

        currentSelected = 0

    End Sub

    Sub ShowInTakes()
        'If ListBox1.Items.Count = 0 Or currentInTake < 0 Then Exit Sub
        'Dim C As New InTake
        'C = CType(ListBox1.Items.Item(currentInTake), InTake)
        'TextName.Text = C.Med
        'TextQnty.Text = C.Qnty
        'TextDate.Text = C.TDate

        ' Should open new form to view/edit according to current item selected type

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxData.SelectedIndexChanged
        If ListBoxData.SelectedIndex > -1 Then
            currentSelected = ListBoxData.SelectedIndex
        End If
    End Sub

    Private Sub ButtonAdd_Click(sender As Object, e As EventArgs) Handles ButtonAdd.Click
        If (currentObjectType.Equals("UserStock")) Then
            Dim oForm As New Stock_Add_Form
            oForm.setUserId(UserId)
            oForm.setForm(Me)
            oForm.Show()

        ElseIf (currentObjectType.Equals("UserTakes")) Then
            Dim oForm As New InTake_Add_Form
            oForm.setUserId(UserId)
                oForm.setForm(Me)
                oForm.Show()
            End If



        ' Should open new form to add according to current item selected type

    End Sub

    Private Sub ButtonGetInTakes_Click(sender As Object, e As EventArgs) Handles ButtonGetInTakes.Click
        currentObjectType = "UserTakes"
        Label1.Text = "InTakes"
        ButtonAdd.Enabled = True
        LoadData()
    End Sub

    Private Sub ButtonGetStock_Click(sender As Object, e As EventArgs) Handles ButtonGetStock.Click
        currentObjectType = "UserStock"
        Label1.Text = "Stock"
        ButtonAdd.Enabled = True
        LoadData()
    End Sub

    Private Sub ButtonGetPrescriptions_Click(sender As Object, e As EventArgs) Handles ButtonGetPrescriptions.Click
        currentObjectType = "UserPrescriptions"
        Label1.Text = "Prescriptions"
        ButtonAdd.Enabled = False
        LoadData()
    End Sub

    Private Sub ButtonGetApointments_Click(sender As Object, e As EventArgs) Handles ButtonGetApointments.Click
        currentObjectType = "UserAppointments"
        Label1.Text = "Appointments"
        ButtonAdd.Enabled = False
        LoadData()
    End Sub

    Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            Remove()
        End If
    End Sub

    Sub Remove()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "EXEC dbo.spRemove" + currentObjectType + " " + Convert.ToString(UserId)

        If (currentObjectType.Equals("UserStock")) Then
            CMD.CommandText += ", '" + ListBoxData.Items(currentSelected).Name + "'"
        ElseIf (currentObjectType.Equals("UserTakes")) Then
            CMD.CommandText += ", '" + ListBoxData.Items(currentSelected).Med + "', '" + ListBoxData.Items(currentSelected).TDate + "'"
        ElseIf (currentObjectType.Equals("UserPrescriptions")) Then
            CMD.CommandText += "," + ListBoxData.Items(currentSelected).ToString.Split(",")(0).Split(":")(1)
        ElseIf (currentObjectType.Equals("UserAppointments")) Then
            CMD.CommandText += ", '" + Convert.ToString(ListBoxData.Items(currentSelected).AppointmentId) + "'"
        End If

        'MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As String
        RDR = CMD.ExecuteScalar

        CN.Close()

        LoadData()

    End Sub

    Public Overrides Sub Refresh()
        LoadData()
    End Sub

    Private Sub ButtonEdit_Click(sender As Object, e As EventArgs) Handles ButtonEdit.Click

        If (currentObjectType.Equals("UserTakes")) Then
            Dim C As New InTake
            C = CType(ListBoxData.Items.Item(currentSelected), InTake)
            Dim oForm As New Intake_Form
            oForm.setUserId(UserId)
            oForm.setObj(C)
            oForm.setForm(Me)
            oForm.Show()
        ElseIf (currentObjectType.Equals("UserStock")) Then
            Dim C As New Stock
            C = CType(ListBoxData.Items.Item(currentSelected), Stock)
            Dim oForm As New Stock_Form
            oForm.setUserId(UserId)
            oForm.setObj(C)
            oForm.setForm(Me)
            oForm.Show()

        ElseIf (currentObjectType.Equals("UserPrescriptions")) Then
            Dim C As New Prescription
            C = CType(ListBoxData.Items.Item(currentSelected), Prescription)
            Dim oForm As New Prescription_Form
            oForm.setUserId(UserId)
            oForm.setObj(C)
            oForm.setForm(Me)
            oForm.Show()

        ElseIf (currentObjectType.Equals("UserAppointments")) Then
            Dim C As New Appointment
            C = CType(ListBoxData.Items.Item(currentSelected), Appointment)
            Dim oForm As New Appointment_Form
            oForm.setUserId(UserId)
            oForm.setObj(C)
            oForm.setForm(Me)
            oForm.Show()

        End If

    End Sub

    Private Sub ListBoxData_DoubleClick(sender As Object, e As EventArgs) Handles ListBoxData.DoubleClick
        ButtonEdit.PerformClick()
    End Sub

    Private Sub ButtonStatistics_Click(sender As Object, e As EventArgs) Handles ButtonStatistics.Click
        Dim StatisticsForm As New FormStatistics
        StatisticsForm.setUserId(UserId)
        StatisticsForm.Show()
    End Sub

    Private Sub ButtonSupervisees_Click(sender As Object, e As EventArgs) Handles ButtonSupervisees.Click
        Dim Sup_Form As New Supervisee_form
        Sup_Form.setUserId(UserId)
        Sup_Form.Show()
    End Sub

    Public Sub setNewFormAsSupervised()
        ButtonSupervisees.Enabled = False
        ButtonDelUser.Enabled = False
    End Sub

    Public Sub setNewFormAsNormal()
        ButtonSupervisees.Enabled = True
        ButtonDelUser.Enabled = True
    End Sub

    Private Sub ButtonDelUser_Click(sender As Object, e As EventArgs) Handles ButtonDelUser.Click
        If (MsgBox("Are you sure? ", MsgBoxStyle.YesNo, "Confirmation") = MsgBoxResult.Yes) Then
            DelUser()
        End If
    End Sub

    Private Sub DelUser()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        ''-------------------------------------------------------

        CMD.CommandText = "EXEC dbo.spRemoveUser " + Convert.ToString(UserId)

        'MsgBox(CMD.CommandText)

        CN.Open()

        Dim RDR As String
        RDR = CMD.ExecuteScalar

        CN.Close()

        Me.Close()
    End Sub

    'Private Sub ButtonRemove_Click(sender As Object, e As EventArgs) Handles ButtonRemove.Click
    '    Dim C As New InTake
    '    C = CType(ListBox1.Items.Item(currentInTake), InTake)
    '    Dim oForm As New Intake_Form
    '    oForm.setUserId(UserId)
    '    oForm.setInTake(C)
    '    oForm.Show()
    'End Sub
End Class
