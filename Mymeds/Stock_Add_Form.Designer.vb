﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Stock_Add_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_price = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_validity = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_formula = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_unq_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_Name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_expiration = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBoxMedicines = New System.Windows.Forms.ListBox()
        Me.txt_take_qnty = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_hour = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ButtonCancel = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ButtonAdd = New System.Windows.Forms.Button()
        Me.txt_qnty = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBoxMedSearch = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txt_price
        '
        Me.txt_price.Location = New System.Drawing.Point(442, 171)
        Me.txt_price.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_price.Name = "txt_price"
        Me.txt_price.ReadOnly = True
        Me.txt_price.Size = New System.Drawing.Size(264, 20)
        Me.txt_price.TabIndex = 46
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(319, 171)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "Medicine Price:"
        '
        'txt_validity
        '
        Me.txt_validity.Location = New System.Drawing.Point(442, 206)
        Me.txt_validity.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_validity.Name = "txt_validity"
        Me.txt_validity.ReadOnly = True
        Me.txt_validity.Size = New System.Drawing.Size(264, 20)
        Me.txt_validity.TabIndex = 44
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(319, 206)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(119, 13)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Medicine Validity(Days):"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(440, 19)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 42
        Me.Label7.Text = "Medicine Info"
        '
        'txt_formula
        '
        Me.txt_formula.Location = New System.Drawing.Point(442, 135)
        Me.txt_formula.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_formula.Name = "txt_formula"
        Me.txt_formula.ReadOnly = True
        Me.txt_formula.Size = New System.Drawing.Size(264, 20)
        Me.txt_formula.TabIndex = 41
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(319, 135)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 13)
        Me.Label6.TabIndex = 40
        Me.Label6.Text = "Medicine Formula:"
        '
        'txt_unq_name
        '
        Me.txt_unq_name.Location = New System.Drawing.Point(442, 93)
        Me.txt_unq_name.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_unq_name.Name = "txt_unq_name"
        Me.txt_unq_name.ReadOnly = True
        Me.txt_unq_name.Size = New System.Drawing.Size(264, 20)
        Me.txt_unq_name.TabIndex = 39
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(319, 93)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Medicine Unique Name:"
        '
        'txt_Name
        '
        Me.txt_Name.Location = New System.Drawing.Point(442, 54)
        Me.txt_Name.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_Name.Name = "txt_Name"
        Me.txt_Name.ReadOnly = True
        Me.txt_Name.Size = New System.Drawing.Size(264, 20)
        Me.txt_Name.TabIndex = 37
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(319, 54)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(84, 13)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Medicine Name:"
        '
        'txt_expiration
        '
        Me.txt_expiration.Location = New System.Drawing.Point(442, 241)
        Me.txt_expiration.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_expiration.Name = "txt_expiration"
        Me.txt_expiration.ReadOnly = True
        Me.txt_expiration.Size = New System.Drawing.Size(264, 20)
        Me.txt_expiration.TabIndex = 35
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(319, 245)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Expiration Date:"
        '
        'ListBoxMedicines
        '
        Me.ListBoxMedicines.FormattingEnabled = True
        Me.ListBoxMedicines.Location = New System.Drawing.Point(9, 60)
        Me.ListBoxMedicines.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ListBoxMedicines.Name = "ListBoxMedicines"
        Me.ListBoxMedicines.Size = New System.Drawing.Size(281, 407)
        Me.ListBoxMedicines.TabIndex = 47
        '
        'txt_take_qnty
        '
        Me.txt_take_qnty.Location = New System.Drawing.Point(442, 347)
        Me.txt_take_qnty.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_take_qnty.Name = "txt_take_qnty"
        Me.txt_take_qnty.Size = New System.Drawing.Size(264, 20)
        Me.txt_take_qnty.TabIndex = 51
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(319, 347)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 50
        Me.Label1.Text = "Take Quantity:"
        '
        'txt_hour
        '
        Me.txt_hour.Location = New System.Drawing.Point(442, 314)
        Me.txt_hour.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_hour.Name = "txt_hour"
        Me.txt_hour.Size = New System.Drawing.Size(264, 20)
        Me.txt_hour.TabIndex = 49
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(319, 314)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 48
        Me.Label9.Text = "Take hour:"
        '
        'ButtonCancel
        '
        Me.ButtonCancel.Location = New System.Drawing.Point(321, 406)
        Me.ButtonCancel.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonCancel.Name = "ButtonCancel"
        Me.ButtonCancel.Size = New System.Drawing.Size(158, 60)
        Me.ButtonCancel.TabIndex = 52
        Me.ButtonCancel.Text = "Cancel"
        Me.ButtonCancel.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 44)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 54
        Me.Label10.Text = "Medicines:"
        '
        'ButtonAdd
        '
        Me.ButtonAdd.Location = New System.Drawing.Point(546, 406)
        Me.ButtonAdd.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ButtonAdd.Name = "ButtonAdd"
        Me.ButtonAdd.Size = New System.Drawing.Size(158, 60)
        Me.ButtonAdd.TabIndex = 55
        Me.ButtonAdd.Text = "Add"
        Me.ButtonAdd.UseVisualStyleBackColor = True
        '
        'txt_qnty
        '
        Me.txt_qnty.Location = New System.Drawing.Point(442, 278)
        Me.txt_qnty.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.txt_qnty.Name = "txt_qnty"
        Me.txt_qnty.Size = New System.Drawing.Size(264, 20)
        Me.txt_qnty.TabIndex = 57
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(319, 280)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 13)
        Me.Label11.TabIndex = 56
        Me.Label11.Text = "Stock Quantity:"
        '
        'TextBoxMedSearch
        '
        Me.TextBoxMedSearch.Location = New System.Drawing.Point(9, 19)
        Me.TextBoxMedSearch.Name = "TextBoxMedSearch"
        Me.TextBoxMedSearch.Size = New System.Drawing.Size(281, 20)
        Me.TextBoxMedSearch.TabIndex = 58
        '
        'Stock_Add_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 476)
        Me.Controls.Add(Me.TextBoxMedSearch)
        Me.Controls.Add(Me.txt_qnty)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.ButtonAdd)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.ButtonCancel)
        Me.Controls.Add(Me.txt_take_qnty)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_hour)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.ListBoxMedicines)
        Me.Controls.Add(Me.txt_price)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txt_validity)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_formula)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_unq_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_Name)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_expiration)
        Me.Controls.Add(Me.Label2)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Stock_Add_Form"
        Me.Text = "Stock_Add_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_price As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txt_validity As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txt_formula As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_unq_name As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_Name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_expiration As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ListBoxMedicines As ListBox
    Friend WithEvents txt_take_qnty As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txt_hour As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents ButtonCancel As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents ButtonAdd As Button
    Friend WithEvents txt_qnty As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBoxMedSearch As TextBox
End Class
