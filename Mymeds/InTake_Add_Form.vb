﻿Imports System.Data.SqlClient

Public Class InTake_Add_Form
    Dim CN As SqlConnection
    Dim CMD As SqlCommand
    Dim currentSelected As Integer
    Dim UserId As Integer
    Dim oForm As Form1

    Friend Sub setUserId(last As Integer)
        UserId = last
    End Sub

    Friend Sub setForm(f As Form1)
        oForm = f
    End Sub

    Private Sub InTake_Add_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "Select * FROM dbo.getUserStock ( " + Convert.ToString(UserId) + " )"

        Dim RDR As SqlDataReader
        CN.Open()
        RDR = CMD.ExecuteReader

        ListBoxMedicines.Items.Clear()

        While RDR.Read
            Dim M As New Medicine
            M.Unique_name = RDR.Item("MEDICINE_UNIQUE_NAME")
            M.Name = RDR.Item("MEDICINE_NAME")
            M.Price = RDR.Item("PRICE")
            M.Formula = RDR.Item("MEDICINE_FORMULA")
            M.Validity = RDR.Item("VALIDITY")
            ListBoxMedicines.Items.Add(M)
        End While
        CN.Close()
        currentSelected = 0
        showMed()
    End Sub

    Sub showMed()
        If ListBoxMedicines.Items.Count = 0 Or currentSelected < 0 Then Exit Sub
        Dim med As New Medicine
        med = CType(ListBoxMedicines.Items.Item(currentSelected), Medicine)
        txt_Name.Text = med.Name
        txt_unq_name.Text = med.Unique_name
        txt_formula.Text = med.Formula
        Dim d As DateTime
        d = DateTime.Now.AddDays(med.Validity)
        txt_expiration.Text = d.ToString("yyyy-MM-dd")
    End Sub

    Private Sub ButtonCancel_Click(sender As Object, e As EventArgs) Handles ButtonCancel.Click
        Me.Close()
    End Sub

    Private Sub ButtonAdd_Click(sender As Object, e As EventArgs) Handles ButtonAdd.Click
        CMD = New SqlCommand
        CMD.Connection = CN
        CMD.CommandText = "EXEC dbo.spAddUserInTake " + Convert.ToString(UserId) + ", '" + txt_unq_name.Text.ToString + "', '" + txt_hour.Text.ToString + "', " + txt_take_qnty.Text.ToString
        CN.Open()

        CMD.ExecuteScalar()

        If CN.State = ConnectionState.Open Then
            CN.Close()
        End If
        oForm.Refresh()
        Me.Close()
    End Sub

    Private Sub ListBoxMedicines_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxMedicines.SelectedIndexChanged
        If ListBoxMedicines.SelectedIndex > -1 Then
            currentSelected = ListBoxMedicines.SelectedIndex
            showMed()
        End If
    End Sub

    Private Sub TextBoxMedSearch_TextChanged(sender As Object, e As EventArgs) Handles TextBoxMedSearch.TextChanged
        limitMedSearch()
    End Sub

    Private Sub limitMedSearch()
        CN = firstPage.getCN()
        CMD = New SqlCommand
        CMD.Connection = CN

        CMD.CommandText = "Select * FROM dbo.UserMedsSearch ( " + Convert.ToString(UserId) + ", '" + TextBoxMedSearch.Text + "' )"

        Dim RDR As SqlDataReader
        CN.Open()

        RDR = CMD.ExecuteReader

        ListBoxMedicines.Items.Clear()

        While RDR.Read
            Dim M As New Medicine
            M.Unique_name = RDR.Item("MEDICINE_UNIQUE_NAME")
            M.Name = RDR.Item("MEDICINE_NAME")
            M.Price = RDR.Item("PRICE")
            M.Formula = RDR.Item("MEDICINE_FORMULA")
            M.Validity = RDR.Item("VALIDITY")
            ListBoxMedicines.Items.Add(M)
        End While
        CN.Close()
        currentSelected = 0
        showMed()
    End Sub
End Class